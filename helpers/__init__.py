import re


USERNAME_RE = re.compile(r'\w{5,21}', flags=re.ASCII)
PASSWORD_RE = re.compile(r'\w{5,21}', flags=re.ASCII)


def check_username(username):
    if re.match(USERNAME_RE, username) is not None:
        return username


def check_password(password):
    if re.match(PASSWORD_RE, password) is not None:
        return password


def fibonacci(n):
    if n <= 1:
        return 1
    return fibonacci(n - 1) + fibonacci(n - 2)


def ewma(time_series, t):
    if t == 0:
        return time_series[-1]

    n = len(time_series)
    a = 2/(n + 1)

    return a*time_series[n-t-1] + (1 - a)*ewma(time_series, t-1)

