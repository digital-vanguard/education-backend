from operator import itemgetter
from random import shuffle
from datetime import timedelta

from flask import Flask, request, jsonify
from flask_cors import CORS, cross_origin
from flask_jwt_extended import (
    JWTManager, jwt_required, create_access_token, get_jwt_identity)
from flask_pymongo import PyMongo
from passlib.hash import pbkdf2_sha512 as hasher

from config import Config
from helpers import check_username, check_password, fibonacci, ewma


app = Flask(__name__, static_folder=None)
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'
app.config.from_object(Config)
jwt = JWTManager(app)
mongo = PyMongo(app)


@app.errorhandler(500)
def internal_server_error(error):
    return jsonify(error='Internal server error')


@app.errorhandler(401)
def not_authorized_error(error):
    return jsonify(error='Unauthorized')


@app.route('/register', methods=['POST'])
@cross_origin()
def register():
    if not request.is_json:
        return jsonify(error='Request must be application/json'), 400

    access_token_expiration_time = timedelta(days=1)

    username = check_username(request.json['username'])
    password = check_password(request.json['password'])

    if not (username and password):
        return jsonify(error='Invalid username or password'), 400

    user = mongo.db.users.find_one(filter={'username': username})
    if user:
        return jsonify(error='User with username \'{}\' already exists'.format(username)), 400

    password_hash = hasher.hash(password)
    subjects = list()
    for subject in mongo.db.subjects.find():
        name = subject['name']
        categories = [category['name'] for category in subject['categories']]
        subjects.append({
            'name': name,
            'overall': 0,
            'categories': list(),
        })
        current_subject = subjects[-1]
        for category in categories:
            current_subject['categories'].append({
                'name': category,
                'score': 0,
                'time_series': list(),
            })
    mongo.db.users.insert({
        'username': username,
        'password_hash': password_hash,
        'fighter_level': 1,
        'current_exp': 0,
        'subjects': subjects,
    })
    access_token = create_access_token(identity=username, expires_delta=access_token_expiration_time)
    return jsonify(access_token=access_token), 200


@app.route('/login', methods=['POST'])
@cross_origin()
def login():
    if not request.is_json:
        return jsonify(error='Request must be application/json'), 400

    access_token_expiration_time = timedelta(days=1)

    username = check_username(request.json['username'])
    password = check_password(request.json['password'])

    if not (username and password):
        return jsonify(error='Invalid username or password'), 400

    user = mongo.db.users.find_one(filter={'username': username})
    if not user:
        return jsonify(error='User with username \'{}\' doesn\'t exist'.format(username)), 400

    access_token = create_access_token(identity=username, expires_delta=access_token_expiration_time)
    return jsonify(access_token=access_token), 200


@app.route('/account', methods=['GET'])
@cross_origin()
@jwt_required
def account():
    username = get_jwt_identity()
    user = mongo.db.users.find_one(filter={'username': username})
    del user['password_hash']
    del user['_id']
    return jsonify(user), 200


@app.route('/training', methods=['GET'])
@cross_origin()
def training():
    username = request.args['username']
    subject_name = request.args['subject']
    user = mongo.db.users.find_one(filter={'username': username})
    if not user:
        return jsonify(error='User with username \'{}\' doesn\'t exist'.format(username)), 400

    found_subject = None
    for subj in user['subjects']:
        if subj['name'] == subject_name:
            found_subject = subj
            break

    if not found_subject:
        return jsonify(error='Couldn\'t find subject: {}'.format(subject_name)), 400

    categories = [
        (category['name'], category['score'])
        for category in found_subject['categories']
    ]
    categories = [
        category_name
        for category_name, category_score
        in sorted(categories, key=itemgetter(1))
    ]
    categories = categories[:5]

    proposed_tasks = list()
    subject = mongo.db.subjects.find_one(filter={'name': subject_name})
    for category in subject['categories']:
        if category['name'] in categories:
            tasks = category['tasks']
            shuffle(tasks)
            task = tasks[0]
            task['category'] = category['name']
            proposed_tasks.append(task)

    return jsonify(tasks=proposed_tasks), 200


@app.route('/battle', methods=['GET'])
@cross_origin()
def battle():
    username_1 = request.args['username_1']
    username_2 = request.args['username_2']
    subject_name = request.args['subject']

    user_1 = mongo.db.users.find_one(filter={'username': username_1})
    user_2 = mongo.db.users.find_one(filter={'username': username_2})
    if not user_1:
        return jsonify(error='User with username \'{}\' doesn\'t exist'.format(username_1)), 400
    if not user_2:
        return jsonify(error='User with username \'{}\' doesn\'t exist'.format(username_2)), 400

    found_subject_1 = None
    for subj in user_1['subjects']:
        if subj['name'] == subject_name:
            found_subject_1 = subj
            break

    found_subject_2 = None
    for subj in user_2['subjects']:
        if subj['name'] == subject_name:
            found_subject_2 = subj
            break

    if not (found_subject_1 and found_subject_2):
        return jsonify(error='Couldn\'t find specified subject'), 400

    categories = dict()
    for category in found_subject_1['categories']:
        categories[category['name']] = category['score']

    for category in found_subject_2['categories']:
        categories[category['name']] -= category['score']
        categories[category['name']] = abs(categories[category['name']])

    categories = list(map(itemgetter(0), sorted(categories.items(), key=itemgetter(1))))[:5]

    proposed_tasks = list()
    subject = mongo.db.subjects.find_one(filter={'name': subject_name})
    for category in subject['categories']:
        if category['name'] in categories:
            tasks = category['tasks']
            shuffle(tasks)
            task = tasks[0]
            task['category'] = category['name']
            proposed_tasks.append(task)

    return jsonify(tasks=proposed_tasks), 200


@app.route('/experience/<int:exp>', methods=['POST'])
@cross_origin()
@jwt_required
def add_experience(exp):
    username = get_jwt_identity()
    user = mongo.db.users.find_one(filter={'username': username})
    current_level = user['fighter_level']
    current_exp = user['current_exp'] + exp

    while fibonacci(current_level) <= current_exp:
        current_exp -= fibonacci(current_level)
        current_level += 1

    user['current_exp'] = current_exp
    user['fighter_level'] = current_level

    mongo.db.users.replace_one(
        {'username': username},
        user,
        upsert=True)

    return jsonify(status='success'), 200


@app.route('/time_series', methods=['POST'])
@cross_origin()
@jwt_required
def time_series():
    if not request.is_json:
        return jsonify(error='Request must be application/json'), 400

    username = get_jwt_identity()
    user = mongo.db.users.find_one(filter={'username': username})

    user_subjects = user['subjects']
    given_subject = request.json['subject']

    for user_subject in user_subjects:
        if given_subject['name'] == user_subject['name']:
            for task in given_subject['tasks']:
                for category in user_subject['categories']:
                    if task['category'] == category['name']:
                        category['time_series'].append(int(task['correctness']))
                        if len(category['time_series']) > 10:
                            category['time_series'].pop(0)

    for user_subject in user_subjects:
        if user_subject['name'] == given_subject['name']:
            for category in user_subject['categories']:
                if len(category['time_series']) > 0:
                    category['score'] = round(100 * ewma(category['time_series'], len(category['time_series'])-1))

    new_overall = 0
    for user_subject in user_subjects:
        if user_subject['name'] == given_subject['name']:
            for category in user_subject['categories']:
                new_overall += category['score']
            new_overall /= len(user_subject['categories'])
            user_subject['overall'] = new_overall

    mongo.db.users.replace_one(
        {'username': username},
        user,
        upsert=True)

    return jsonify(status='success'), 200
